# chat

#### 项目介绍
netty-socketio 消息推送，聊天室

#### 软件架构
1.SpringBoot 2.0+
2.Freemarker
3.socket.io.js
4.moment.min.js
5.jquery-1.10.1.min.js


#### 演示地址

1. http://45.77.195.211:1024/chat

#### 参考

1. https://github.com/mrniko/netty-socketio-demo

#### 截图
![聊天截图](https://images.gitee.com/uploads/images/2018/1018/154859_b6f6e0c8_893181.png "(A0UO8{PON0~BEUR6(1KL)6.png")