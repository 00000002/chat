//path
function getRoot() {
    var hostname = location.hostname;
    var pathname = location.pathname;
    var contextPath = pathname.split("/")[1];
    var port = location.port;
    var protocol = location.protocol;
    return protocol + "//" + hostname + ":" + port + "/";
}
var BASE_URL = getRoot();

/**
 * 使用message对象封装消息 
 */
var message = {
	time : 0,
	title : document.title,
	timer : null,
	//通知
	notification:function(msg){
		Notification.requestPermission(function(permission) {
			console.log(permission);
			if (permission == "granted") {
				var notification = new Notification("您有一条新的消息", {
					dir : "auto",
					lang : "zh-CN",
					tag : "testNotice",
					icon : BASE_URL+'static/img/msg.png',
					body : '您有一条新的消息:'+(msg!=null?msg:'')
				});
			}
		})
	},
	// 显示新消息提示  
	show : function() {
		var title = message.title.replace("【　　　】", "").replace("【新消息】", "");
		// 定时器，设置消息切换频率闪烁效果就此产生  
		message.timer = setTimeout(function() {
			message.time++;
			message.show();
			if (message.time % 2 == 0) {
				document.title = "【新消息】" + title
			}

			else {
				document.title = "【　　　】" + title
			}
			;
		}, 600);
		return [ message.timer, message.title ];
	},
	// 取消新消息提示  
	clear : function() {
		clearTimeout(message.timer);
		document.title = message.title;
	}
};
//message.notification();
// 页面加载时绑定点击事件，单击取消闪烁提示  
function bind() {
	document.onclick = function() {
		message.clear();
	};
}