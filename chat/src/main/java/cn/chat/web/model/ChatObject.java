package cn.chat.web.model;

import java.util.List;
import java.util.Map;

public class ChatObject {

	private String sessionId;
    private String userName;
    private String message;
    private Integer count;
    private List<Map<String,Object>> userList;

    public ChatObject(String userName, String message,String sessionId,Integer count,List<Map<String,Object>> uesrList) {
        super();
        this.userName = userName;
        this.message = message;
        this.sessionId = sessionId;
        this.count = count;
        this.userList = uesrList;
    }

	public List<Map<String, Object>> getUserList() {
		return userList;
	}


	public void setUserList(List<Map<String, Object>> userList) {
		this.userList = userList;
	}


	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public ChatObject() {
    }

    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }

}
