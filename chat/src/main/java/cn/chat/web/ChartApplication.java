package cn.chat.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import cn.chat.web.core.ChartServer;

@SpringBootApplication
public class ChartApplication implements CommandLineRunner{

	@Autowired
	private ChartServer chartServer;
	
	@Value("${chat.port}")
	private Integer chatPort;
	
	public static void main(String[] args) {
		SpringApplication.run(ChartApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println("netty启动端口："+chatPort);
		chartServer.run(chatPort);
	}
}
