/**    
* @Title: ChartServer.java  
* @Package cn.ctx.web.core  
* @Description: TODO(用一句话描述该文件做什么)  
* @author gyu
* @date 2018年10月11日 上午10:09:41  
* @version V1.0    
*/
package cn.chat.web.core;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.corundumstudio.socketio.AckRequest;
import com.corundumstudio.socketio.Configuration;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.listener.ConnectListener;
import com.corundumstudio.socketio.listener.DataListener;
import com.corundumstudio.socketio.listener.DisconnectListener;

import cn.chat.web.model.ChatObject;
import cn.chat.web.service.ClientService;
import cn.chat.web.service.MsgService;

/**  
* @ClassName: ChartServer  
* @Description: TODO(netty 服务)  
* @author gyu
* @date 2018年10月11日 上午10:09:41  
*/
@Component
public class ChartServer {
    public SocketIOServer server;
    
    @Autowired
    private ClientService clientService;
    
    @Autowired
    private MsgService msgService;
    
    private final static String CHANNEL = "chatevent";
    
	public void run(Integer port) throws InterruptedException {

        Configuration config = new Configuration();
//        config.setHostname("localhost");
        config.setPort(port);
        final SocketIOServer server = new SocketIOServer(config);
        setServer(server);
        server.addEventListener(CHANNEL, ChatObject.class, new DataListener<ChatObject>() {
            @Override
            public void onData(SocketIOClient client, ChatObject data, AckRequest ackRequest) {
                client.set(client.getSessionId().toString(), data.getUserName());
                data.setSessionId(client.getSessionId().toString());
                data.setUserList(clientService.onlineClientAll());
                clientService.saveCurentClient(client);
                msgService.sendMsgToAll(data);
            }
        });
        
        server.addDisconnectListener(new DisconnectListener() {
			@Override
			public void onDisconnect(SocketIOClient client) {
				System.err.println("客户端关闭链接:\n"+client.getSessionId());
				clientService.delCurentClient(client);
                ChatObject data=new ChatObject();
                data.setSessionId(client.getSessionId().toString());
                data.setCount(clientService.onlineClientCount());
                data.setUserList(clientService.onlineClientAll());
                msgService.sendMsgToAll(data);
			}
        	
        });
        
        server.addConnectListener(new ConnectListener() {
			@Override
			public void onConnect(SocketIOClient client) {
				//TODO 可以用初始化用户信息到client中
                clientService.saveCurentClient(client);
                ChatObject data=new ChatObject();
                data.setSessionId(client.getSessionId().toString());
                data.setCount(clientService.onlineClientCount());
                data.setUserList(clientService.onlineClientAll());
                msgService.sendMsgToAll(data);
			}
		});
        start();
        
	}
	
	public void setServer(SocketIOServer server) {
		this.server=server;
	}
	
	public SocketIOServer getServer() {
		return server;
	}

	public void start() {
		server.start();
		System.err.println("netty服务已启动");
	}
	
	public void stop() {
        server.stop();
		System.err.println("netty服务已关闭");
	}
	
	public String getChannel() {
		return CHANNEL;
	}
	
	public void printAllUser() {
        System.out.println("在线用户数量:"+clientService.onlineClientCount());
		List<Map<String,Object>> list = clientService.onlineClientAll();
		for(Iterator<Map<String,Object>> it=list.iterator();it.hasNext();) {
			Map<String,Object> map=it.next();
			for (Entry<String, Object> entry : map.entrySet()) {
				System.out.println("用户id:"+entry.getKey());
				System.out.println("用户:"+ entry.getValue());
	        }
		}
	}
}
