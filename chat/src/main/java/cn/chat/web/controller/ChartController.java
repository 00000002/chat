package cn.chat.web.controller;
/**    
* @Title: TestController.java  
* @Package cn.ctx.admin.controller  
* @Description: TODO(用一句话描述该文件做什么)  
* @author gyu
* @date 2017年10月31日 下午3:41:47  
* @version V1.0    
*/


import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.corundumstudio.socketio.SocketIOClient;

import cn.chat.web.model.ChatObject;
import cn.chat.web.service.ClientService;
import cn.chat.web.service.MsgService;

/**  
* @ClassName: AdminController  
* @Description: TODO(推送接口)  
* @author gyu
* @date 2017年10月31日 下午3:41:47  
*    
*/
@Controller
public class ChartController {
	
	@Autowired
	private MsgService msgService;
	
	@Autowired
	private ClientService clientService;
	
	/**
	* @Title: index
	* @Description: TODO(聊天页面)
	* @author gyu
	* @date 2018年10月11日下午3:50:56
	 */
	@RequestMapping(value = "/chat", produces = "application/json; charset=utf-8")
	public Object index(Model model) {
		model.addAttribute("count", clientService.onlineClientCount());
		return "chat/index";
	}

	/**
	* @Title: sendToUser
	* @Description: TODO(私聊)
	* @param sessionId
	* @param ChatObject
	* @author gyu
	* @date 2018年10月11日下午3:51:08
	 */
	@RequestMapping(value = "/sendToUser", produces = "application/json; charset=utf-8")
	@ResponseBody
	public Object sendToUser(String sessionId,ChatObject chatObject) {
		msgService.sendMsgToUser(sessionId, chatObject);
		SocketIOClient client=clientService.getClientBySessionId(sessionId);
		Map<String,Object> result = new HashMap<String,Object>();
		result.put("state", "success");
		result.put("msg", "发送,"+client.getNamespace().getName()+",成功");
		return result;
	}
	
	/**
	* @Title: sendToAll
	* @Description: TODO(广播)
	* @param ChatObject
	* @author gyu
	* @date 2018年10月11日下午3:48:32
	 */
	@RequestMapping(value = "/sendToAll", produces = "application/json; charset=utf-8")
	@ResponseBody
	public Object sendToAll(ChatObject chatObject) {
		msgService.sendMsgToAll(chatObject);
		Map<String,Object> result = new HashMap<String,Object>();
		result.put("state", "success");
		result.put("msg", "发送所有用户成功");
		return result;
	}

	/**
	* @Title: closeBySessionId
	* @Description: TODO(踢人)
	* @param sessionId
	* @author gyu
	* @date 2018年10月11日下午3:51:34
	 */
	@RequestMapping(value = "/closeBySessionId", produces = "application/json; charset=utf-8")
	@ResponseBody
	public Object closeBySessionId(String sessionId) {
		Map<String,Object> result = new HashMap<String,Object>();
		String userName=clientService.closeBySessionId(sessionId);
		result.put("state", "success");
		result.put("msg", "你已经把"+userName+"踢出群聊");
		return result;
	}
}
