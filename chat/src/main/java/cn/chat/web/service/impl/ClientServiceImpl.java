/**    
* @Title: UserServiceImpl.java  
* @Package cn.ctx.web.service.impl  
* @Description: TODO(用一句话描述该文件做什么)  
* @author gyu
* @date 2018年10月11日 上午10:39:03  
* @version V1.0    
*/
package cn.chat.web.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.stereotype.Service;

import com.corundumstudio.socketio.SocketIOClient;

import cn.chat.web.service.ClientService;

/**  
* @ClassName: UserServiceImpl  
* @Description: TODO(连接实现)  
* @author gyu
* @date 2018年10月11日 上午10:39:03  
*    
*/
@Service
public class ClientServiceImpl implements ClientService{
	public ConcurrentHashMap<String,SocketIOClient> currentMap=new ConcurrentHashMap<String,SocketIOClient>();
	@Override
	public void saveCurentClient(SocketIOClient client) {
		currentMap.put(client.getSessionId().toString(), client);
	}


	@Override
	public void delCurentClient(SocketIOClient client) {
		currentMap.remove(client.getSessionId().toString());
	}


	@Override
	public List<Map<String, Object>> onlineClientAll() {
		List<Map<String, Object>> list=new ArrayList<Map<String, Object>>();
		Map<String,Object> map=null;
		for (Map.Entry<String, SocketIOClient> entry : currentMap.entrySet()) {
            map=new HashMap<String,Object>();
            map.put(entry.getKey(), entry.getValue().get(entry.getKey()));
            list.add(map);
        }
		return list;
	}

	@Override
	public int onlineClientCount() {
		return currentMap.size();
	}


	@Override
	public SocketIOClient getClientBySessionId(String sessionId) {
		return currentMap.get(sessionId);
	}

	@Override
	public String closeBySessionId(String sessionId) {
		SocketIOClient client=getClientBySessionId(sessionId);
		if(null == client) {
			return "";
		}
		String userName=client.get(sessionId);
		client.disconnect();
		return userName;
	}
	

}
