/**    
* @Title: MsgServiceImpl.java  
* @Package cn.ctx.web.service.impl  
* @Description: TODO(用一句话描述该文件做什么)  
* @author gyu
* @date 2018年10月11日 上午10:39:13  
* @version V1.0    
*/
package cn.chat.web.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.corundumstudio.socketio.SocketIOClient;

import cn.chat.web.core.ChartServer;
import cn.chat.web.model.ChatObject;
import cn.chat.web.service.ClientService;
import cn.chat.web.service.MsgService;

/**  
* @ClassName: MsgServiceImpl  
* @Description: TODO(消息实现)  
* @author gyu
* @date 2018年10月11日 上午10:39:13  
*    
*/
@Service
public class MsgServiceImpl implements MsgService{

	@Autowired
	private ChartServer chartServer;
	
	@Autowired
	private ClientService clientService;
	
	
	@Override
	public void sendMsgToUser(String sessionId,ChatObject chatObject) {
		SocketIOClient client=clientService.getClientBySessionId(sessionId);
		chartServer.getServer().getClient(client.getSessionId()).sendEvent(chartServer.getChannel(), chatObject);
	}

	@Override
	public void sendMsgToAll(ChatObject chatObject) {
		chartServer.getServer().getBroadcastOperations().sendEvent(chartServer.getChannel(), chatObject);
	}

}
