/**    
* @Title: MsgService.java  
* @Package cn.ctx.web.service  
* @Description: TODO(用一句话描述该文件做什么)  
* @author gyu
* @date 2018年10月11日 上午10:36:32  
* @version V1.0    
*/
package cn.chat.web.service;

import cn.chat.web.model.ChatObject;

/**  
* @ClassName: MsgService  
* @Description: TODO(这里用一句话描述这个类的作用)  
* @author gyu
* @date 2018年10月11日 上午10:36:32  
*    
*/
public interface MsgService {

	void sendMsgToUser(String sessionId,ChatObject chatObject);
	
	void sendMsgToAll(ChatObject chatObject);
	
}
