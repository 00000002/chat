/**    
* @Title: UserService.java  
* @Package cn.ctx.web.service  
* @Description: TODO(用一句话描述该文件做什么)  
* @author gyu
* @date 2018年10月11日 上午10:32:25  
* @version V1.0    
*/
package cn.chat.web.service;

import java.util.List;
import java.util.Map;

import com.corundumstudio.socketio.SocketIOClient;

/**  
* @ClassName: UserService  
* @Description: TODO(这里用一句话描述这个类的作用)  
* @author gyu
* @date 2018年10月11日 上午10:32:25  
*    
*/
public interface ClientService {

	/**
	* @Title: saveCurentClient
	* @Description: TODO(保存当前连接)
	* @param SocketIOClient
	* @author gyu
	* @date 2018年10月11日上午10:33:58
	 */
	void saveCurentClient(SocketIOClient client);
	
	/**
	* @Title: onlineClientAll
	* @Description: TODO(所有在线用户)
	* @author gyu
	* @date 2018年10月11日上午10:35:02
	 */
	List<Map<String,Object>> onlineClientAll();
	
	/**
	* @Title: onlineClientCount
	* @Description: TODO(在线链接数)
	* @author gyu
	* @date 2018年10月11日上午10:58:55
	 */
	int onlineClientCount();
	
	/**
	* @Title: delCurentClient
	* @Description: TODO(删除当天链接)
	* @param SocketIOClient
	* @author gyu
	* @date 2018年10月11日上午10:35:47
	 */
	void delCurentClient(SocketIOClient client);
	
	/**
	* @Title: getClientBySessionId
	* @Description: TODO(根据SessionId获取一个链接)
	* @param sessionId
	* @author gyu
	* @date 2018年10月11日上午11:03:58
	 */
	SocketIOClient getClientBySessionId(String sessionId);
	
	/**
	* @Title: closeBySessionId
	* @Description: TODO(踢人)
	* @param sessionId
	* @author gyu
	* @date 2018年10月11日下午3:53:11
	 */
	String closeBySessionId(String sessionId);
}
